This is Google Chrome or Chromium app for https://ticktick.com.

*How to install*

1. Download or clone files from repo: https://bitbucket.org/kibal4iw/ticktickchromeapp/src

2. Go to chrome://extensions/ (just copy and paste in chrome omnibox or menu -> Tools -> Extensions)

3. Enable Developer mode by ticking the checkbox in the upper-right corner.

4. Click on the "Load unpacked extension..." button

5. Select the directory containing your unpacked extension


*Some helpful config*

1. Open chrome://apps/ (just copy and paste in chrome omnibox)

2. Right-click on "Tick Tick Chromium App"

3. Click on "Open in new tab" or "Show Apps in new tab" - it's allow work with TickTick how standalone app

4. Click "Create shortcuts"

Profit :)

Goodluck